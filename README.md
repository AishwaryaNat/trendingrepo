# Github Trending Repositories in Android

An Android App that lists the most trending repositories in Android from Github.

#### App Features
* Users can view the most trending repositories in Android from Github.
* Users can view their rating count.

#### App Architecture 
Based on MVP architecture and repository pattern.

#### App Specs
* [Retrofit 2](https://square.github.io/retrofit/) for API integration.
* [Gson](https://github.com/google/gson) for serialisation.
* [Okhhtp3](https://github.com/square/okhttp) for implementing interceptor, logging and mocking web server.
* [Glide](https://github.com/bumptech/glide) for loading image view.