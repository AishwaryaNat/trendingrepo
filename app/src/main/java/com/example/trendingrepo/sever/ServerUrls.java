package com.example.trendingrepo.sever;

public class ServerUrls {
    /**
     * The constant Base URL.
     */
    public static final String BASE_URL = "https://gtrend.yapie.me/";

    public static final String REPOSITORIES = "repositories";

}
