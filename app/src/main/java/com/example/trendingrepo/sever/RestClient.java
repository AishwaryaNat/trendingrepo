package com.example.trendingrepo.sever;

import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestClient {

    /**
     * This interface is used to access the url methods.
     */
    private Services services;

    /**
     * This method is used to set the rest adapter for retrofit with the time out session and
     * headers for the api call.
     */
    public RestClient() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder client = new OkHttpClient.Builder();
        client.addInterceptor(interceptor)
                .readTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(30, TimeUnit.SECONDS)
                .addNetworkInterceptor((Interceptor) new SessionRequestInterceptor())
                .build();

        //Adding Base url to the retrofit.
        Retrofit restAdapter = getRetrofit(client);

        services = restAdapter.create(Services.class);
    }

    /**
     * Method used to get the Retrofit http Client
     *
     * @param httpClient OkHttpClient.Builder
     * @return Retrofit httpClient
     */
    public static Retrofit getRetrofit(OkHttpClient.Builder httpClient) {
        return new Retrofit.Builder()
                .baseUrl(ServerUrls.BASE_URL)
                .client(httpClient.build())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    /**
     * This is the service interface used to get the service callback methods
     *
     * @return Services
     */
    public Services getServices() {
        return services;
    }
}
