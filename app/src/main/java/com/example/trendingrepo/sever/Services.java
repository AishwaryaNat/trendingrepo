package com.example.trendingrepo.sever;

import com.example.trendingrepo.model.RepoDetail;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface Services {


    @GET(ServerUrls.REPOSITORIES)
    Call<List<RepoDetail>> repoList(@Query("spoken_language_code")
                                            String spokenLanguage);
}
