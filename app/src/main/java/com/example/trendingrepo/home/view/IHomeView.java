package com.example.trendingrepo.home.view;

import com.example.trendingrepo.model.RepoDetail;

import java.util.List;

public interface IHomeView {

    void onSuccessRepoList(List<RepoDetail> repoDetailList);

    void onFailureRepoList();

    void dismissProgress();
}
