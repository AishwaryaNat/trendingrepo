package com.example.trendingrepo.home.presenter;

import android.content.Context;

import com.example.trendingrepo.home.model.HomeModel;
import com.example.trendingrepo.home.model.IHomeModel;
import com.example.trendingrepo.home.view.IHomeView;
import com.example.trendingrepo.model.RepoDetail;

import java.util.List;

public class HomePresenter implements IHomeResponsePresenter, IHomePresenter {

    IHomeView view;
    IHomeModel model;

    public HomePresenter(IHomeView view) {
        this.view = view;
        this.model = new HomeModel(this);
    }

    @Override
    public void getRepoList(Context context) {
        model.getRepoList(context);
    }

    @Override
    public void onSuccessRepoListResponse(List<RepoDetail> repoDetailList) {
        view.onSuccessRepoList(repoDetailList);
    }

    @Override
    public void onFailureRepoListResponse() {
        view.onFailureRepoList();
    }

    @Override
    public void dismissProgress() {
        view.dismissProgress();
    }
}
