package com.example.trendingrepo.home.presenter;

import com.example.trendingrepo.model.RepoDetail;

import java.util.List;

public interface IHomeResponsePresenter {

    void onSuccessRepoListResponse(List<RepoDetail> repoDetailList);

    void onFailureRepoListResponse();

    void dismissProgress();
}
