package com.example.trendingrepo.home.presenter;

import android.content.Context;

public interface IHomePresenter {

    void getRepoList(Context context);
}
