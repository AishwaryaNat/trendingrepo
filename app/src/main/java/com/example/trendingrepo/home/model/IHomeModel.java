package com.example.trendingrepo.home.model;

import android.content.Context;

public interface IHomeModel {

    void getRepoList(Context context);
}
