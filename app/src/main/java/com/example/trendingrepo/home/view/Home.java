package com.example.trendingrepo.home.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.example.trendingrepo.Adapter.RepoAdapter;
import com.example.trendingrepo.R;
import com.example.trendingrepo.app.Logger;
import com.example.trendingrepo.app.Utils;
import com.example.trendingrepo.home.presenter.HomePresenter;
import com.example.trendingrepo.model.RepoDetail;
import com.example.trendingrepo.views.ProgressDialog;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Home extends AppCompatActivity implements IHomeView {

    @BindView(R.id.repo_recycler)
    RecyclerView repoRecycler;

    RepoAdapter repoAdapter;

    ProgressDialog progressDialog;

    HomePresenter homePresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        homePresenter = new HomePresenter(this);

        if (Utils.checkNetworkAndShowDialog(this)) {
            homePresenter.getRepoList(this);
        }

    }

    @Override
    public void onSuccessRepoList(List<RepoDetail> repoDetailList) {
        repoAdapter = new RepoAdapter(repoDetailList, this);
        repoRecycler.setLayoutManager(new LinearLayoutManager(this));
        repoRecycler.setAdapter(repoAdapter);
    }

    @Override
    public void onFailureRepoList() {
        Logger.showShortMessage(this,"Please Try Again");
    }

    @Override
    public void dismissProgress() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }
}