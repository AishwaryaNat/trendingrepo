package com.example.trendingrepo.home.model;

import android.content.Context;

import androidx.annotation.NonNull;

import com.example.trendingrepo.app.AppController;
import com.example.trendingrepo.home.presenter.IHomeResponsePresenter;
import com.example.trendingrepo.model.RepoDetail;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeModel implements IHomeModel {

    IHomeResponsePresenter presenter;

    public HomeModel(IHomeResponsePresenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void getRepoList(Context context) {

        Call<List<RepoDetail>> getRepoListApi = AppController.getRestClient()
                .getServices().repoList("english");

        getRepoListApi.enqueue(new Callback<List<RepoDetail>>() {
            @Override
            public void onResponse(@NonNull Call<List<RepoDetail>> call,
                                   @NonNull Response<List<RepoDetail>> response) {

                presenter.dismissProgress();
                if (response.body() != null && response.code() == 200) {
                    presenter.onSuccessRepoListResponse(response.body());
                } else
                    presenter.onFailureRepoListResponse();

            }

            @Override
            public void onFailure(@NonNull Call<List<RepoDetail>> call,
                                  @NonNull Throwable t) {

                presenter.onFailureRepoListResponse();
                presenter.dismissProgress();
            }
        });
    }
}
