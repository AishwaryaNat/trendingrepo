package com.example.trendingrepo.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.trendingrepo.R;
import com.example.trendingrepo.model.RepoDetail;

import java.util.List;

public class RepoAdapter extends RecyclerView.Adapter<RepoAdapter.RepoHolder> {

    List<RepoDetail> repoList;
    Context context;

    public RepoAdapter(List<RepoDetail> repoList, Context context) {
        this.repoList = repoList;
        this.context = context;
    }

    @NonNull
    @Override
    public RepoHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.repo_item, parent, false);
        return new RepoHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RepoHolder holder, int position) {

        RepoDetail data = repoList.get(position);

        Glide.with(context)
                .load(data.getAvatar())
                .into(holder.avatar);

        holder.repoName.setText(data.getRepoName());
        holder.description.setText(data.getDescription());
        if (data.getLanguageColor() != null)
            holder.langColor.setBackgroundColor(Color.parseColor(data.getLanguageColor()));
        holder.lang.setText(data.getLanguage());
        holder.starCount.setText(String.valueOf(data.getStars()));
    }

    @Override
    public int getItemCount() {
        return repoList.size();
    }

    /**
     * Holder Class
     */
    class RepoHolder extends RecyclerView.ViewHolder {

        ImageView avatar;
        TextView repoName;
        TextView description;
        ImageView langColor;
        TextView lang;
        TextView starCount;

        public RepoHolder(@NonNull View itemView) {
            super(itemView);

            avatar = itemView.findViewById(R.id.avatar);
            repoName = itemView.findViewById(R.id.repo_name);
            description = itemView.findViewById(R.id.description);
            langColor = itemView.findViewById(R.id.lang_color);
            lang = itemView.findViewById(R.id.lang);
            starCount = itemView.findViewById(R.id.star_count);
        }
    }
}
