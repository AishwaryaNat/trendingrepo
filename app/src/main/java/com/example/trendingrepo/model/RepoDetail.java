package com.example.trendingrepo.model;

import com.google.gson.annotations.SerializedName;

public class RepoDetail {

    @SerializedName("name")
    String repoName;

    @SerializedName("description")
    String description;

    @SerializedName("language")
    String language;

    @SerializedName("languageColor")
    String languageColor;

    @SerializedName("stars")
    int stars;

    @SerializedName("avatar")
    String avatar;

    public String getRepoName() {
        return repoName;
    }

    public void setRepoName(String repoName) {
        this.repoName = repoName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getLanguageColor() {
        return languageColor;
    }

    public void setLanguageColor(String languageColor) {
        this.languageColor = languageColor;
    }

    public int getStars() {
        return stars;
    }

    public void setStars(int stars) {
        this.stars = stars;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
}
