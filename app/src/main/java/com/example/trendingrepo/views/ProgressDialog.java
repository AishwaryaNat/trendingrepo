package com.example.trendingrepo.views;

import android.app.Dialog;
import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ProgressBar;

import androidx.core.content.ContextCompat;

import com.example.trendingrepo.R;
import com.example.trendingrepo.app.Logger;

public class ProgressDialog extends Dialog {

    /**
     * The context.
     */
    private Context context;

    /**
     * Default constructor which instantiates a new Loading progress dialog.
     *
     * @param context The context to use.
     */
    public ProgressDialog(Context context) {
        super(context);
        this.context = context;
    }

    /**
     * Method to display the progress dialog calling this will show the dialog instantly visibility
     * needs to be checked by the super class implementation.
     */
    public void showProgress() {
        try {
            View view = LayoutInflater.from(context).inflate(R.layout.dialog_progress_round, null);
            this.requestWindowFeature(Window.FEATURE_NO_TITLE);
            this.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup
                    .LayoutParams.MATCH_PARENT);
            this.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

            this.setCancelable(false);
            this.setContentView(view);
            this.show();
            ProgressBar progressBar = view.findViewById(R.id.progressBar1);
            progressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(context, R.color.colorAccent),
                    PorterDuff.Mode.SRC_IN);
        } catch (Exception e) {
            Logger.logError(e);
        }
    }
}
