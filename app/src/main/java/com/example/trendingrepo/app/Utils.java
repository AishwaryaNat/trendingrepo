package com.example.trendingrepo.app;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.example.trendingrepo.R;

public final class Utils {

    /**
     * To check the internet connectivity and show error message if network not available.
     *
     * @param context Contains the context
     * @return boolean True if internet is available false
     */
    public static boolean checkNetworkAndShowDialog(Context context) {
        if (!checkNetConnection(context)) {
            Logger.showShortMessage(context, context.getString(R.string.check_internet));

            return false;
        }
        return true;
    }

    /**
     * Checking internet state.
     *
     * @param context Activity context
     * @return boolean True if internet is enabled else false
     */
    public static boolean checkNetConnection(Context context) {
        ConnectivityManager miManager = (ConnectivityManager) context.getSystemService(
                Context.CONNECTIVITY_SERVICE);
        NetworkInfo miInfo = miManager.getActiveNetworkInfo();
        boolean networkStatus = false;

        //Checking the network connection is in wifi or mobile data
        if (miInfo != null && miInfo.getType() == ConnectivityManager.TYPE_WIFI) {
            networkStatus = true;
        } else if (miInfo != null && miInfo.getType() == ConnectivityManager.TYPE_MOBILE &&
                miInfo.isConnectedOrConnecting())
            networkStatus = true;
        return networkStatus;
    }
}
