package com.example.trendingrepo.app;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.trendingrepo.R;

public class Logger {

    /**
     * Tag to be presented in the log
     */
    private static final String TAG = "Trending Repo::";

    private static Toast toast = null;


    /**
     * Method to log the error
     *
     * @param e Exception raised by the logger
     */
    public static void logError(Exception e) {
        if (e != null && e.getMessage() != null && !TextUtils.isEmpty(e.getMessage()))
            Log.e(TAG, e.getMessage());
    }

    /**
     * Method to display the Toast message
     *
     * @param context Context
     * @param message Message to show
     */
    public static void showShortMessage(Context context, String message) {
        try {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            View layout = inflater.inflate(R.layout.custom_toast, null);
            TextView text = layout
                    .findViewById(R.id.custom_toast_message);
            text.setText(message);
            if (toast != null)
                toast.cancel();
            toast = new Toast(context);
            toast.setGravity(Gravity.BOTTOM, 0, 160);
            toast.setDuration(Toast.LENGTH_SHORT);
            toast.setView(layout);
            toast.show();
        } catch (Exception e) {
            Logger.logError(e);
        }
    }

}
