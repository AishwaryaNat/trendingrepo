package com.example.trendingrepo.app;

import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;

import com.example.trendingrepo.sever.RestClient;

public class AppController extends MultiDexApplication {

    /**
     * Rest client instance
     */
    private static RestClient restClient;


    /**
     * Getting the rest client access through the context
     *
     * @return RestClient
     */
    public static RestClient getRestClient() {
        if (restClient == null) {
            restClient = new RestClient();
        }
        return restClient;
    }

    /**
     * Initializing the rest client in this application class
     *
     * @param restClient Object of the restClient
     */
    private static void setRestClient(RestClient restClient) {
        AppController.restClient = restClient;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        MultiDex.install(this);
        setRestClient(new RestClient());
    }
}
